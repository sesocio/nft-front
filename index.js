// Routes
import NftRoutes from './src/modules/nft/routes';
import MisNftRoutes from './src/modules/mis-nft/routes';
import NftProjectDetailRoutes from './src/modules/ProjectDetail/routes';
import NftBidRoutes from './src/modules/bid/routes';
import NftCollectionRoutes from './src/modules/collection/routes';

// Stores
import NftStore from './src/modules/nft/store';
import NftProjectDetailStore from './src/modules/ProjectDetail/store';
import NftBidStore from './src/modules/bid/store';
import NftCollectionStore from './src/modules/collection/store';

const routes = [];
const routesNft = routes.concat(
  MisNftRoutes,
  NftRoutes,
  NftProjectDetailRoutes,
  NftBidRoutes,
  NftCollectionRoutes
);

const storeNft = {
  nft: NftStore,
  nft_project_detail: NftProjectDetailStore,
  nft_bid: NftBidStore,
  nft_collection: NftCollectionStore
}

export {
	routesNft,
  storeNft,
};
