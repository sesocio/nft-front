export default [
  {
    path: "/nft/:nft_collection_name_id",
    name: "collections",
    component: () => import(/* webpackChunkName: "Collection" */ '../pages/Collection'),
    title: "Collections"
  }
];