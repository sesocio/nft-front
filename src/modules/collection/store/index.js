import actions from "./actions";
import mutations from "./mutations";

const state = {
  collection: {},
  loading: false,
  allCollections: [],
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};