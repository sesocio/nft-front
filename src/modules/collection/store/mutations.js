const SET_LOADING = (state, payload) => {
  state.loading = payload;
};

const SET_COLLECTION = (state, payload) => {
  state.collection = payload;
};

const SET_ALL_COLLECTIONS = (state, data) => {
  state.allCollections = data;
};

const SET_VOTE_IN_COLLECTION = (state, payload) => {
  if(state.collection.nfts.length > 0){
    let index = state.collection.nfts.findIndex(p => p.id === payload.id);
    state.collection.nfts[index].currentUserLike = true,
    state.collection.nfts[index].countLikes++
  }
};

const REMOVE_VOTE_IN_COLLECTION = (state, payload) => {
  if(state.collection.nfts.length > 0){
    let index = state.collection.nfts.findIndex(p => p.id === payload.id);
    state.collection.nfts[index].currentUserLike = false,
    state.collection.nfts[index].countLikes--
  }
};

const CLEAN_COLLECTION = (state) => {
  state.collection = {};
};

export default {
  SET_LOADING,
  SET_COLLECTION,
  SET_ALL_COLLECTIONS,
  SET_VOTE_IN_COLLECTION,
  REMOVE_VOTE_IN_COLLECTION,
  CLEAN_COLLECTION
}
