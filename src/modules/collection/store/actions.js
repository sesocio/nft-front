import { Project } from "ui-components";
import { NftCollection } from "ui-components";

const getCollection = async ({ commit }, payload) => {
  commit("SET_LOADING", true);

  try {
    let collection = await NftCollection
      .includes('nft_author')
      .where({ 'name_id': payload.nft_collection_name_id })
      .first();

    let nftsByCollection = await Project.select([
      'country', 'title', 'title_id', 'titleId', 'currency', 'name', 'logo_url',
      'project_subtype', 'project_id', 'days_remaining', 'currency_view', 'currencyView',
      'part_value', 'id', 'summary', 'enddate', 'images_url', 'imagesUrl',
      'startDate', 'completion', 'objective', 'project_status_message', 'parts_precision', 'part_value_original'
    ])
      .selectExtra([
        'project_status_message', 'count_likes', 'current_user_like',
        'part_value_plus_step', 'nft_files'
      ])
      .includes(['category', 'nft_collection', 'nft_collection.nft_author'])
      .where({ country: payload.country })
      .where({ project_subtype: ['nft'] })
      .where({ nft_collection_name_id: [payload.nft_collection_name_id] })
      .where({ 'show_currency': 'project_currency' })
      .order({nft_default_order: 'asc'})
      .where({ nft_home: true })
      .all();

    const filterCollection = {
      ...collection.data,
      nfts: nftsByCollection.data
    }
    
    commit("SET_COLLECTION", filterCollection);
    commit("SET_LOADING", false);
  } catch (err) {
    console.log(err)
    commit("SET_LOADING", false);
    return err;
  }
};

const getAllCollections = async ({ commit }, payload) => {
  commit("SET_LOADING", true);

  try {
    let { data } = await NftCollection.all();
    commit("SET_ALL_COLLECTIONS", data);
    commit("SET_LOADING", false);
  } catch (err) {
    console.log("cannot get collections");
    commit("SET_LOADING", false);
    return err;
  }
};

const cleanCollection = async ({ commit }) => {
  commit("CLEAN_COLLECTION");
};

export default {
  getCollection,
  getAllCollections,
  cleanCollection
};