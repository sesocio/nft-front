export default [
  {
    path: "/nft/:nft_collection_name_id/:title_id/offer",
    name: "ofertar",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Index'),
    title: "Ofertar",
    meta: { requiresAuth: true },
  },
  {
    path: "/nft/:nft_collection_name_id/:title_id/offer/confirm",
    name: "confirmar_oferta",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Summary'),
    title: "Confirmar oferta",
    meta: { requiresAuth: true },
  },
  {
    path: "/nft/:nft_collection_name_id/:title_id/offer/outbided",
    name: "outbided",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Outbided'),
    title: "Oferta superada",
    meta: { requiresAuth: true },
  },
  {
    path: "/nft/:nft_collection_name_id/:title_id/offer/congrats",
    name: "congrats",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Congrats'),
    title: "Confirmada",
    meta: { requiresAuth: true },
  }
];
