import { Project } from "ui-components";
import { Checkout } from "ui-components";
import { Investment } from "ui-components";
import Axios from "axios";

const goToStep = ({rootState, commit, dispatch }, step) => {
  commit("SET_STEP", step);
};

const getProject = async ({ commit, dispatch }, json) => {
  commit("SET_LOADING_PROJECT", true);
  try {
    let project = await Project.select(
      {
        projects: [
          'country', 'title', 'title_id', 'currency', 'name', 'logo_url', 'project_subtype',
          'project_id', 'currency_view', 'part_value', 'id'
        ],
        nft_collection: ['name', 'name_id'],
      })
    .includes(['nft_collection', 'nft_collection.nft_author'])
    .where(json).where({'show_currency': 'project_currency'}).first();

    commit("SET_PROJECT", project.data);
    commit("SET_LOADING_PROJECT", false);

  } catch (err) {
    commit("SET_LOADING_PROJECT", false);
    return err;
  }
};

const getCurrencies = async ({ commit, dispatch }, id) => {
  try {
    commit("SET_LOADING_CURRENCIES", true);
    await Axios.get(
      process.env.VUE_APP_API_BASE_URL +
        process.env.VUE_APP_API_VERSION_URL +
        "/investments/get_user_investment_currencies/" + id
      )
      .then(function(resp) {
        const data = resp.data;
        commit("SET_CURRENCIES", data);
        commit("SET_LOADING_CURRENCIES", false);
      })
      .catch(function() {
        commit("SET_LOADING_CURRENCIES", false);
      });
  } catch (error) {
    return error.response;
  }
};

const setSelectedCurrency = ({commit}, currency) => {
  commit("SET_SELECTED_CURRENCY", currency);
};

const setAmount = ({commit}, amount) => {
  commit("SET_AMOUNT", amount);
};

const setCreatingCheckout = ({commit}, payload) => {
  commit("SET_CREATING_CHECKOUT", payload);
};

const setCheckout = async ({ commit, dispatch }, json) => {
  dispatch('setCreatingCheckout', true);
  let checkout = new Checkout();
  checkout.amount = json.amount;
  checkout.currency = json.currency;
  checkout.projectId = json.project_id;
  checkout.fromCollection = 'available_amount';
  await checkout
    .save()
    .then(function(success) {
      if (!success) {
        commit("SET_CHECKOUT", { err: checkout.errors });
      } else {
        commit("SET_CHECKOUT", checkout);
      }
      dispatch('setCreatingCheckout', false);
    })
    .catch(function(e) {
      commit("SET_CHECKOUT", { err: e });
      dispatch('setCreatingCheckout', false);
    });
};

const setCreatingOffer = ({commit}, payload) => {
  commit("SET_CREATING_OFFER", payload);
};

const setOffer = async ({ commit, dispatch }, checkout) => {
  dispatch('setCreatingOffer', true);
  await checkout
    .save()
    .then(function(success) {
      if (!success) {
        commit("SET_OFFER", { errors: checkout.errors });
      } else {
        commit("SET_OFFER", checkout);
        // dispatch("getCurrencies", checkout.projectId);
      }
      dispatch('setCreatingOffer', false);
    })
    .catch(function(e) {
      commit("SET_OFFER", { errors: e.response });
      dispatch('setCreatingOffer', false);
    });
};

const getOfferStatus = async ({ commit, dispatch }, json) => {
  try {
    let invest = await Investment.where({checkout_id: json.checkout}).first();
    commit("SET_OFFER_STATUS", invest.data.status);
  } catch (err) {
    return err;
  }
};

const cleanSelectedCurrency = ({ commit }) => {
  commit("SET_SELECTED_CURRENCY", null);
};

const cleanCheckout = ({ commit }) => {
  commit("SET_CHECKOUT", null);
  commit("SET_OFFER", null);
  commit("SET_OFFER_STATUS", 'processing');
  commit("SET_AMOUNT", null);
  commit("SET_SELECTED_CURRENCY", null);
};

  export default {
    goToStep,
    getProject,
    getCurrencies,
    setSelectedCurrency,
    setAmount,
    setCreatingCheckout,
    setCheckout,
    setCreatingOffer,
    setOffer,
    getOfferStatus,
    cleanSelectedCurrency,
    cleanCheckout
};
