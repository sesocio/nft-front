import actions from "./actions";
import mutations from "./mutations";

const state = {
  steps: {
    currency: true,
    amount: false,
    summary: false,
    confirm: false,
  },
  step: "currency",
  project: [],
  loading_project: true,
  currencies: [],
  loading_currencies: true,
  selected_currency: null,
  amount: null,
  creating_checkout: false,
  checkout: null,
  offer: null,
  creating_offer: false,
  offerStatus: "processing",
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
