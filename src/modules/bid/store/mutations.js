const SET_STEP = (state, step) => {
  for (var s in state.steps) {
    if(s == step){
      state.steps[s] = true;
    } else {
      state.steps[s] = false;
    }
  }
  if(step == undefined){
    step = "currency";
  }
  state.step = step;
};

const SET_PROJECT = (state, data) => {
  state.project = data;
};

const SET_LOADING_PROJECT = (state, payload) => {
  state.loading_project = payload;
};

const SET_CURRENCIES = (state, data) => {
  state.currencies = data;
};

const SET_LOADING_CURRENCIES = (state, payload) => {
  state.loading_currencies = payload;
};

const SET_SELECTED_CURRENCY = (state, payload) => {
  state.selected_currency = payload;
};

const SET_AMOUNT = (state, payload) => {
  state.amount = payload;
};

const SET_CREATING_CHECKOUT = (state, payload) => {
  state.creating_checkout = payload;
};

const SET_CHECKOUT = (state, payload) => {
  state.checkout = payload;
};

const SET_CREATING_OFFER = (state, payload) => {
  state.creating_offer = payload;
};

const SET_OFFER = (state, payload) => {
  state.offer = payload;
};

const SET_OFFER_STATUS = (state, payload) => {
  state.offerStatus = payload;
};

export default {
  SET_STEP,
  SET_PROJECT,
  SET_LOADING_PROJECT,
  SET_CURRENCIES,
  SET_LOADING_CURRENCIES,
  SET_SELECTED_CURRENCY,
  SET_AMOUNT,
  SET_CREATING_CHECKOUT,
  SET_CHECKOUT,
  SET_CREATING_OFFER,
  SET_OFFER,
  SET_OFFER_STATUS
};
