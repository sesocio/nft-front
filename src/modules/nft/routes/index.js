export default [
  {
    path: "/nft",
    name: "nft",
    component: () => import(/* webpackChunkName: "Home" */ '../pages/Home'),
    title: "NFT"
  }
];
