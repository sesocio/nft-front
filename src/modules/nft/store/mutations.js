const SET_NFTS_PROJECTS = (state, data) => {
  state.nftsProjects = data;
};

const SET_LOADING_NFTS = (state, payload) => {
  state.loadingnfts = payload;
};

const SET_CATEGORIES_NFT = (state, payload) => {
  state.categoriesNfts = payload;
};

const SET_VOTE = (state, payload) => {
  if(state.nftsProjects.length > 0){
    let index = state.nftsProjects.findIndex(p => p.id === payload.id);
    state.nftsProjects[index].currentUserLike = true,
    state.nftsProjects[index].countLikes++
  }
};

const REMOVE_VOTE = (state, payload) => {
  if(state.nftsProjects.length > 0){
    let index = state.nftsProjects.findIndex(p => p.id === payload.id);
    state.nftsProjects[index].currentUserLike = false,
    state.nftsProjects[index].countLikes--
  }
};

const SET_TOTAL_ITEMS = (state, payload) => {
  state.totalItems = payload;
};

const SET_CURRENT_PAGE = (state, payload) => {
  state.currentPage = payload;
};

const SET_NFTS_LIST_VOTE = (state, payload) => {
  state.nftsListVote = payload;
};

  export default {
    SET_LOADING_NFTS,
    SET_NFTS_PROJECTS,
    SET_CATEGORIES_NFT,
    SET_NFTS_LIST_VOTE,
    SET_TOTAL_ITEMS,
    SET_CURRENT_PAGE,
    SET_VOTE,
    REMOVE_VOTE
  };
