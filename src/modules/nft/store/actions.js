import { Project } from "ui-components";
import { Category } from "ui-components";

import Axios from "axios";

const getNftsProjects = async ({ commit }, payload) => {
  commit("SET_LOADING_NFTS", true);
  let page = payload ? payload.page : 1;
  let perPage = 8;
  try {
    let { data } = await Project.select([
      'country', 'title', 'title_id', 'titleId', 'currency', 'name', 'logo_url',
      'project_subtype', 'project_id', 'days_remaining', 'currency_view',
      'currencyView', 'part_value', 'id', 'summary', 'enddate', 'images_url',
      'imagesUrl', 'startDate', 'completion', 'objective', 'project_status_message',
      'parts_precision', 'order_by_votes', 'status', 'part_value_original'
    ])
      .selectExtra([
        'project_status_message', 'count_likes', 'current_user_like',
        'part_value_plus_step', 'nft_files', 'order_by_votes'
      ])
      .includes(['category', 'nft_collection', 'nft_collection.nft_author'])
      .where({ country: payload.country })
      .where({ 'show_currency': 'project_currency' })
      .where({ project_subtype: ['nft'] })
      .where({ category_id: payload.categories })
      .where({ nft_home: true })
      .order(payload.order)
      .page(page)
      .per(perPage)
      .all();

    commit("SET_NFTS_PROJECTS", data);

    if (page == 1) {
      commit("SET_CURRENT_PAGE", page);

      let scopeCount = await Project
        .includes(['category', 'nft_collection', 'nft_collection.nft_author'])
        .where({ country: payload.country })
        .where({ 'show_currency': 'project_currency' })
        .where({ project_subtype: ['nft'] })
        .where({ category_id: payload.categories })
        .where({ nft_home: true })
        .per(0).stats({
          total: "count"
        })
        .all();

      const totalItems = scopeCount.meta.stats.total.count;
      commit("SET_TOTAL_ITEMS", totalItems);
    }

    commit('SET_LOADING_NFTS', false);
  } catch (err) {
    commit('SET_LOADING_NFTS', false);
    return err;
  }
};

const getNftsCategories = async ({ commit }) => {
  try {
    // 18 es el id de la categoria NFT
    const { data } = await Category
      .where({ category_id_or_id: 18 })
      .per(8)
      .all();

    // Elimina el id 18 de la lista
    const notNftCategory = data.filter(cat => cat.id != 18);

    commit("SET_CATEGORIES_NFT", notNftCategory);

  } catch (err) {
    return err;
  }
};

const sendVote = async ({ commit }, payload) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/projects/vote`;

      const response = await Axios.post(url, {
        project_id: payload.project.id,
        type: 'like',
        value: true
      })

      if (payload.module == "nft_project_detail") {
        commit('nft_project_detail/SET_VOTE_IN_DETAIL', payload.project, { root: true });
      } else if (payload.module == "nft_collection") {
        commit('nft_collection/SET_VOTE_IN_COLLECTION', payload.project, { root: true });
      } else {
        commit('SET_VOTE', payload.project);
      }

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const removeVote = async ({ commit }, payload) => {
  return new Promise(async (resolve, reject) => {
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/projects/vote`;

      const response = await Axios.post(url, {
        project_id: payload.project.id,
        type: 'like',
        value: false
      })

      if (payload.module == "nft_project_detail") {
        commit('nft_project_detail/REMOVE_VOTE_IN_DETAIL', payload.project, { root: true });
      } else if (payload.module == "nft_collection") {
        commit('nft_collection/REMOVE_VOTE_IN_COLLECTION', payload.project, { root: true });
      } else {
        commit('REMOVE_VOTE', payload.project)
      }

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getNftsVote = async ({ commit }, payload) => {
  commit("SET_LOADING_NFTS", true);
  try {
    let { data } = await Project.select({
      project: [
        'count_likes', 'title', 'title_id', 'name', 'id', 'images_url'
      ],
    })
      .selectExtra(['count_likes', 'title_id', 'current_user_like', 'nft_files'])
      .includes(['category', 'nft_collection'])
      .where({ project_subtype: ['nft'] })
      .where({ country: payload.country })
      .where({ nft_home: true })
      .order({ order_by_votes: 'desc' })
      .per(10)
      .all();

    // De los 10, sólo muestro los que tienen votos
    const filterData = data.filter(nft => nft.countLikes > 0);

    commit("SET_NFTS_LIST_VOTE", filterData);
    commit("SET_LOADING_NFTS", false);
  } catch (err) {
    console.log(err);
    commit("SET_LOADING_NFTS", false);
    return err;
  }
};

export default {
  getNftsCategories,
  getNftsProjects,
  getNftsVote,
  sendVote,
  removeVote
};