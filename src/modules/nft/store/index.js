import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const state = {
  currentPage: 1,
  totalItems: 0,
  loadingnfts: true,
  nftsProjects: [],
  categoriesNfts:[],
  nftsListVote: [],
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};