const currentPage = state => state.currentPage;

export default {
    currentPage,
}