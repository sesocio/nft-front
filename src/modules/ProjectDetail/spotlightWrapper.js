// Basado en la librería https://github.com/nextapps-de/spotlight

import __Spotlight from "spotlight.js/src/js/spotlight.js";
import "spotlight.js/dist/css/spotlight.min.css";

export const Spotlight = __Spotlight;
