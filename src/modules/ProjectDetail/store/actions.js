import { Project } from "ui-components";
import Axios from 'axios';

const getProject = async ({ commit }, json) => {
  commit("SET_LOADING", true);
  try {
    let { data } = await Project.select({
      project: [
        'country', 'title', 'title_id', 'currency', 'name', 'logo_url',
        'project_subtype', 'project_id', 'currency_view', 'part_value',
        'id', 'summary', 'extra_summary', 'video', 'enddate', 'images_url',
        'content_title_1','content_detail_1',
        'content_title_2','content_detail_2',
        'content_title_3','content_detail_3',
        'content_title_4','content_detail_4',
        'content_title_5','content_detail_5',
        'content_title_6','content_detail_6',
        'status',
      ],
      nft_collection: ['name', 'name_id', 'logo_url'],
    })
      .selectExtra({
        projects: [
          'article_id', 'part_value_plus_step', 'nft_files',
          'nft_owner', 'count_likes', 'current_user_like'
        ]
      })
      .where({ 'show_currency': 'project_currency' })
      .where(json)
      .includes(['category', 'nft_collection', 'nft_collection.nft_author'])
      .first();

      commit("SET_PROJECT", data);
      commit("SET_LOADING", false);
  } catch (err) {
    commit("SET_LOADING", false);
    return err;
  }
};

const getRecommendedProjects = async ({ commit }, country) => {
  commit("SET_LOADING", true);
  try {
    let { data } = await Project.select({
      projects: [
      'country', 'title', 'title_id', 'currency', 'name', 'logo_url',
      'project_subtype', 'project_id', 'currency_view', 'part_value',
      'id', 'summary', 'enddate', 'images_url', 'start_date'
      ],
      nft_collection: ['name', 'name_id'],
    })
      .selectExtra({
        projects: [
          'project_status_message', 'part_value_plus_step', 'nft_files',
          'count_likes', 'current_user_like'
        ]
      })
      .includes(['category', 'nft_collection'])
      .where({ 'show_currency': 'project_currency' })
      .where({ country: country })
      .where({ project_subtype: ['nft'] })
      .where({ nft_home: true })
      .per(7)
      .all();

    commit("SET_RECOMMENDED_PROJECTS", data);
    commit("SET_LOADING", false);
  } catch (err) {
    commit("SET_LOADING", false);
    return err;
  }
};

// Historial de operaciones
// Copiado de SPA/modules/trading/components/tradingInfo/TradingHistory
const getTradingHistoryByProjectId = async ({ commit }, projectId) => {
  const url = 
    process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL;
  
  try {
    const { data } = await Axios.get(
      `${url}/projects/get_historical_prices/${projectId}`
    );
    return data.historical_trading;
  } catch (err) {
    console.error( err );
    return [];
  }
};

const cleanLoadedData = ({ commit }) => {
  commit("CLEAN_LOADED_DATA");
};

export default {
  getProject,
  getRecommendedProjects,
  getTradingHistoryByProjectId,
  cleanLoadedData
};
