import actions from "./actions";
import mutations from "./mutations";

const state = {
  project: {},
  recommendedProjects: [],
  loading: true,
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
