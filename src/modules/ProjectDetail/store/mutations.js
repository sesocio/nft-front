const SET_PROJECT = (state, data) => {
  state.project = data;
};

const SET_LOADING = (state, data) => {
  state.loading = data;
};

const SET_RECOMMENDED_PROJECTS = (state, data) => {
  state.recommendedProjects = data;
};

const SET_VOTE_IN_DETAIL = (state, payload) => {
  let index = null;

  if (state.recommendedProjects.length) {
    index = state.recommendedProjects.findIndex(p => p.id === payload.id);
    if (index != -1) {
      state.recommendedProjects[index].currentUserLike = true;
      state.recommendedProjects[index].countLikes++;
    }
  }
  if (state.project.id == payload.id) {
    state.project.currentUserLike = true;
    state.project.countLikes++;
  }
};

const REMOVE_VOTE_IN_DETAIL = (state, payload) => {
  let index = null;

  if (state.recommendedProjects.length) {
    index = state.recommendedProjects.findIndex(p => p.id === payload.id);
    if (index != -1) {
      state.recommendedProjects[index].currentUserLike = false;
      state.recommendedProjects[index].countLikes--;
    }
  }
  if (state.project.id == payload.id) {
    state.project.currentUserLike = false;
    state.project.countLikes--;
  }
};

const CLEAN_LOADED_DATA = (state) => {
  state.project = {};
};

export default {
  SET_PROJECT,
  SET_LOADING,
  SET_RECOMMENDED_PROJECTS,
  SET_VOTE_IN_DETAIL,
  REMOVE_VOTE_IN_DETAIL,
  CLEAN_LOADED_DATA
};
