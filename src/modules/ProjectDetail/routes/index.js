export default [
  {
    path: "/nft/:nft_collection_name_id/:title_id",
    name: "NftProject",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Index'),
    title: "NftProject"
  }
];