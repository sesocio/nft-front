const SET_NFT_CATEGORIES = (state, payload) => {
  state.nftCategories = payload;
};

const SET_NFT_PORTFOLIO = (state, payload) => {
  state.nftPortfolios = payload;
};

const SET_TOTAL_NFT_PORTFOLIO = (state, payload) => {
  state.totalNftPortfolio = payload;
};

const UPDATE_NFT_PROJECT_STATUS = (state, payload) => {
  state.nftProjectStatus = payload;
};

const UPDATE_NFT_PROJECT_CATEGORIES = (state, payload) => {
  state.nftProjectCategories = payload;
};

const LOADING_TRUE = (state) => {
  state.loading = true;
};

const LOADING_FALSE = (state) => {
  state.loading = false;
};

export default {
  SET_NFT_CATEGORIES,
  SET_NFT_PORTFOLIO,
  SET_TOTAL_NFT_PORTFOLIO,
  UPDATE_NFT_PROJECT_STATUS,
  UPDATE_NFT_PROJECT_CATEGORIES,
  LOADING_TRUE,
  LOADING_FALSE
};
