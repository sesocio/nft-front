const nftCollections = (state) => {
  let nftCollections = [];
  // get unique collections
  for (let i = 0; i < state.nftPortfolios.length; i++) {
    const portfolio = state.nftPortfolios[i];
    let index = nftCollections.findIndex(
      (nftCollection) => nftCollection.id == portfolio.project.nftCollection.id
    )
    if (index == -1) {
      portfolio.project.nftCollection['portfolios'] = [];
      nftCollections.push(portfolio.project.nftCollection);
    }
  }

  // add projects to collections
  for (let i = 0; i < state.nftPortfolios.length; i++) {
    const portfolio = state.nftPortfolios[i];
    let index = nftCollections.findIndex(
      (nftCollection) => nftCollection.id == portfolio.project.nftCollection.id
    )
    if (index != -1) {
      nftCollections[index]['portfolios'].push(portfolio);
    }
  }

  return nftCollections
}

export default {
  nftCollections
}