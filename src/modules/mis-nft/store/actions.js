import { Portfolio } from "ui-components";
import { Category } from "ui-components";

const getNftCategories = async ({ commit}) => {
  return new Promise(async(resolve, reject)=>{
    try {
      const response = await Category
      .where({
        'category_id_or_id': 18, //id --> nft
      })
      .all();
      commit("SET_NFT_CATEGORIES", response.data);

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getNftPortfolio = async ({ state, commit }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      const response = await Portfolio.select({
        projects: [
          'title', 'images_url', 'currency', 'part_value',
          'title_id', 'status', 'category_id', 'show_currency',
          'enddate'
        ],
        portfolios: [
          'id'
        ]
      })
      .selectExtra({
        projects: [
          'nft_files'
        ]
      })
      .includes("project")
      .includes({
        project: [
          'nft_collection',
          'nft_collection.nft_author'
        ]
      })
      .includes("project_trading")
      .where({
        project_category_id: state.nftProjectCategories, // [ 18 ] --> nft
        project_status: state.nftProjectStatus, // confirmed || pending
      })
      .where({
        show_currency: 'project_currency'
      })
      .all();
      commit("SET_NFT_PORTFOLIO", response.data);

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getTotalNftPortfolio = async ({ state, commit }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      const response = await Portfolio.select({
        projects: [
          'title', 'images_url', 'currency', 'part_value',
          'title_id', 'status', 'category_id', 'show_currency',
          'enddate'
        ],
        portfolios: ['id']
      })
      .where({ project_category_id: 18 }) // 18 es la categoria de nft
      .where({ show_currency: 'project_currency' })
      .stats({ total: "count" })
      .all();
      
      const total = response.meta.stats.total.count

      commit("SET_TOTAL_NFT_PORTFOLIO", total);
      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const updateNftProjectStatus = ({ commit }, data) => {
  commit("UPDATE_NFT_PROJECT_STATUS", data);
};

const updateNftProjectCategories = ({ commit }, data) => {
  commit("UPDATE_NFT_PROJECT_CATEGORIES", data);
};

const loadingTrue = ({ commit }, data) => {
  commit("LOADING_TRUE");
};

const loadingFalse = ({ commit }, data) => {
  commit("LOADING_FALSE");
};

export default {
  getNftCategories,
  getNftPortfolio,
  getTotalNftPortfolio,
  updateNftProjectStatus,
  updateNftProjectCategories,
  loadingTrue,
  loadingFalse
};