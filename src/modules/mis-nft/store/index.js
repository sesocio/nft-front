import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

const state = {
  loading: true,
  nftCategories: [],
  nftPortfolios: [],
  nftProjectStatus: 'confirmed', // confirmed || pending
  nftProjectCategories: [ process.env.VUE_APP_PROJECT_CATEGORIES_ID_NFT ], // 18 --> nft
  totalNftPortfolio: 0, // Total de nfts en el portfolio (sin filtrar)
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};