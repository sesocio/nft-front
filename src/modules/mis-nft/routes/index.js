export default [
  {
    path: "/mis-nft",
    name: "mis-nft",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/MisNft.vue'),
    title: "Mis NFT",
    meta: { requiresAuth: true, layout: 'profile-layout' }
  }
];
